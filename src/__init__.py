import boto.dynamodb2
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.types import NUMBER
from boto.dynamodb2.layer1 import DynamoDBConnection

conn = DynamoDBConnection(
	aws_access_key_id='foo',         # Dummy access key
	aws_secret_access_key='bar',     # Dummy secret key
	host='localhost',                # Host where DynamoDB Local resides
	port=8000,                       # DynamoDB Local port (8000 is the default)
	is_secure=False)                 # Disable secure connections

users = Table.create('users', schema=[
	HashKey('account_type', data_type=NUMBER),
	RangeKey('last_name'),
	], throughput={
	'read': 5,
	'write': 15,
	}, indexes=[
	AllIndex('EverythingIndex', parts=[
	HashKey('account_type', data_type=NUMBER),
	])
],
# List all local tables
tables = conn.list_tables()